/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  


  /**
   * `UserController.new()`
   */
  new: function (req, res) {
    res.view();
  },
  signin: function (req, res) {
    res.view();
  },

  /**
   * `UserController.crereate()`
   */
  create: function(req,res,next){
    User.create(req.params.all(),function userCreated(err,user){
      if(err) {
        console.log(err);
        req.session.flash  = {err:err }
        return res.redirect('user/new'); 
        }
      res.redirect('user/showuser/'+user.id);
    });
  },

  showuser: function(req,res,next){
    User.findOne(req.param('id'),function founduser(err,user){
        if(err) return next(err);
        if(!user) return next();
        res.view({user:user});
    });
  },
  userhome: function(req,res,next){
    User.findOne(req.param('id'),function founduser(err,user){
        if(err) return next(err);
        if(!user) return next();
        res.view({user:user});
    });
  },

  login:function(req,res,next){
    if(!req.param('email') || !req.param('password')){
      var missingErr = [{name: 'usernamePasswordMissingError', message:'Enter both email and password' }];
      req.session.flash ={
        err: missingErr
      }
      res.redirect('user/signin');
      return;
    }
    User.findOneByEmail(req.param('email'),function founduser(err,user){
      if(err) return next(err);
      if(!user){
        var notFoundErr = [{name:'emailAccountNotFound', message:'Email account not registered..'}];
        req.session.flash ={
          err : notFoundErr
        }
        res.redirect('user/signin');
        return;

      }
      // if email is correct
      if(user.password == req.param('password'))
        res.redirect('user/userhome/'+user.id);
      else {
        var wrongPassword = [{name:'wrongPassword', message:'Wrong Combination!'}];
        req.session.flash ={
          err : wrongPassword
        }
        res.redirect('user/signin');
        return;
      }
    });
  },

};

